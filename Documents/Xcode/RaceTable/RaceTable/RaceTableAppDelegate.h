//
//  RaceTableAppDelegate.h
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceTableAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
