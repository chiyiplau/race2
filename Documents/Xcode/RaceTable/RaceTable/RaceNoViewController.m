//
//  RaceNoViewController.m
//  RaceTable
//
//  Created by Chiyip on 31/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import "RaceNoViewController.h"
#import "RaceTableViewCell.h"
#import "RaceTableViewController.h"
#import "RaceDetailViewController.h"
@interface RaceNoViewController ()

@end

@implementation RaceNoViewController



NSIndexPath *path;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _DateLabel = @[@"2013-09-16",
                     @"2013-09-23",
                     @"2013-09-30",
                     @"2013-10-07",
                     @"2013-10-14",
                      @"2013-10-21",
                      @"2013-10-28",
                      @"2013-11-04"];
    _racerNames = @[@"Kester Siu",
                    @"Axel Schmidt",
                    @"Felix Santiago",
                    @"Mark Kuchnick",
                    @"T.C. Chow",
                    @"Angus Ting",
                    @"C.W.  Mak",
                    @"Paul Cooper",
                    @"Nick Cardozo",
                    @"Brett Sandvik"];

    _horseNames = @[@"1",
                    @"2",
                    @"3",
                    @"4",
                    @"5",
                    @"6",
                    @"7",
                    @"8",
                    @"9",
                    @"10"];
    _racerImages = @[@"1_MChadwick.png",
                     @"2_ODoleuze.png",
                     @"3_WCMarwing.png",
                     @"4_ZPurton.jpg",
                     @"5_THSo.png",
                     @"6_KCLeung.png",
                     @"7_CYLui.png",
                     @"8_TAngland.png",
                     @"9_KTeetn.png",
                     @"10_DWhyte.png"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.DateLabel count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"racerTableCell";
    RaceTableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier
                             forIndexPath:indexPath];

    
    // Configure the cell...
    
    long row = [indexPath row];
    
    cell.DateLabel.text = self.DateLabel[row];
    
    return cell;

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowRaceDetails"])
    {
        RaceTableViewController *detailTableViewController =
        [segue destinationViewController];
        
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        //NSIndexPath *myIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        detailTableViewController.raceDetailModel = [[NSArray alloc]
                                                 initWithObjects:[self.racerNames objectAtIndex:[myIndexPath row]],
                                                     [self.racerCountry objectAtIndex:[myIndexPath row]],
                                                     [self.racerImages objectAtIndex:[myIndexPath row]],
                                                 nil];
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
