//
//  RaceDetailViewController.m
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import "RaceDetailViewController.h"
#import "RaceTableViewController.h"
@interface RaceDetailViewController ()

@end

@implementation RaceDetailViewController

@synthesize racerNames = _racerNames;
@synthesize racerCountry = _racerCountry;
@synthesize racerDescription = _racerDescription;
@synthesize imageView = _imageView;
@synthesize racerDetailModel = _racerDetailModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
 
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.racerNames.text = [self.racerDetailModel objectAtIndex:0];
    self.racerCountry.text = [self.racerDetailModel objectAtIndex:1];
    self.racerDescription.text = [self.racerDetailModel objectAtIndex:2];
    self.imageView.image = [UIImage imageNamed:[self.racerDetailModel objectAtIndex:3]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 200, 20)];
    label.font = [UIFont boldSystemFontOfSize:14.0f];  //UILabel的字体大小
    label.numberOfLines = 0;  //必须定义这个属性，否则UILabel不会换行
    label.textAlignment = NSTextAlignmentLeft;  //文本对齐方式
    
    
    //宽度不变，根据字的多少计算label的高度
    NSString *str = self.racerDescription.text;
    CGSize size= [str sizeWithFont:label.font constrainedToSize:CGSizeMake(label.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    //根据计算结果重新设置UILabel的尺寸
    [label setFrame:CGRectMake(40, 220, 250, size.height)];
    label.text = str;
    
    [self.view addSubview:label];
  

    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
