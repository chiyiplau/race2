//
//  RaceNoViewController.h
//  RaceTable
//
//  Created by Chiyip on 31/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceNoViewController : UITableViewController
@property (nonatomic, strong) NSArray *DateLabel;
@property (nonatomic, strong) NSArray *racerImages;
@property (nonatomic, strong) NSArray *racerDescription;
@property (nonatomic, strong) NSArray *racerCountry;
@property (nonatomic, strong) NSArray *racerNames;
@property (nonatomic, strong) NSArray *horseNames;
@end
