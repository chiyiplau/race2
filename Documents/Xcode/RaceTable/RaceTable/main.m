//
//  main.m
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RaceTableAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RaceTableAppDelegate class]));
    }
}
