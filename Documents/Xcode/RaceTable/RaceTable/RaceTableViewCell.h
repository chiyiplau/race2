//
//  RaceTableViewCell.h
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIImageView *racerImages;
@property (nonatomic, strong) IBOutlet UILabel *racerNames;
@property (nonatomic, strong) IBOutlet UILabel *horseNames;

@property (nonatomic, strong) IBOutlet UILabel *DateLabel;


@end
