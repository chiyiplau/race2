//
//  RaceTableViewController.m
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import "RaceTableViewController.h"
#import "RaceTableViewCell.h"
#import "RaceDetailViewController.h"

@interface RaceTableViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation RaceTableViewController
@synthesize racerLabel = _racerLabel;
@synthesize horseLabel = _horseLabel;
@synthesize imageView = _imageView;
@synthesize raceDetailModel = _raceDetailModel;

NSIndexPath *path;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.racerLabel.text = [self.raceDetailModel objectAtIndex:0];
    self.horseLabel.text = [self.raceDetailModel objectAtIndex:1];
    self.imageView.image = [UIImage imageNamed:[self.raceDetailModel objectAtIndex:2]];
    
    _racerImages = @[@"1_MChadwick.png",
                     @"2_ODoleuze.png",
                     @"3_WCMarwing.png",
                     @"4_ZPurton.jpg",
                     @"5_THSo.png",
                     @"6_KCLeung.png",
                     @"7_CYLui.png",
                     @"8_TAngland.png",
                     @"9_KTeetn.png",
                     @"10_DWhyte.png"];
    
    _racerNames = @[@"Kester Siu",
                    @"Axel Schmidt",
                    @"Felix Santiago",
                    @"Mark Kuchnick",
                    @"T.C. Chow",
                    @"Angus Ting",
                    @"C.W.  Mak",
                    @"Paul Cooper",
                    @"Nick Cardozo",
                    @"Brett Sandvik"];
    
    _horseNames = @[@"1",
                    @"2",
                    @"3",
                    @"4",
                    @"5",
                    @"6",
                    @"7",
                    @"8",
                    @"9",
                    @"10"];
    
    _racerCountry = @[@"Hong Kong",
                      @"Austria",
                      @"South Africa",
                      @"Australia",
                      @"Hong Kong",
                      @"Hong Kong",
                      @"Hong Kong",
                      @"New Zealand",
                      @"South Africa",
                      @"New Zealand"];
    
    _racerDescription = @[@"Age 26, Siu began was an Apprentice Jockey and trained in New Zealand.                Siu now trains with one of the territory’s top stables and has claimed  the champion apprentice title.  A bright  start for this confident, promising young rider",
                     @"Age 41, cool, calm and intimidating veteran with an impressive track record in Germany and Europe before settling in Hong Kong.  Consistently a top 10 ranking in Hong Kong over the past eight years.    Goes by the nickname “Axe Man.",
                     @"Age 38; ranked consistently in the top 10 of the South African Jockeys’ Championship, Santiago was a racing legend in his home country.  A string of international successes prior to Hong Kong, in South Africa, Mauritius, Zimbabwe, Japan and Singapore.",
                     @"Age 32, handsome Aussie with big wins down under and Europe before coming to Hong Kong. Friendly and easy-going, Kuchnick is well-loved by fans and other jockeys.  Impressive career win tally and a Royal Ascot victory for this seasoned rider.",
                     @"Age 31, top local rider who toughed his way through the dog-eat-dog local training program.  Got a bit of a chip on his shoulder which actually seems to help him.  Chow is aggressive and cocky, and coming off his best season ever.",
                     @"Age 28; popular local rider.  A top performer at the end of the 2012/13 season.  Hailing from one of Hong Kong’s legendary stables, Ting recently made Hong Kong proud by winning the prestigious Silver Saddle as a top rider at Ascot. ",
                     @"Age 24,  Mak was trained in Australia.  He has won the prestigious apprentice jockeys’ championship and the Asian Young Guns Challenge in Singapore.  Mak finished 2012/13 with career total of 122.  One of Hong Kong’s rising stars, keep an eye on this one",
                     @"Age 39; a perennial favourite on the Hong Kong scene,  Cooper is closing in on an astounding 1,000 wins in Hong Kong,  and had his own record season last year.  Though younger riders are nipping at his heels, never count Cooper out.",
                     @"Age 32; Cardozo logged many notable wins in South Africa, Singapore and the Middle East before arriving in Hong Kong.  An injury sidetracked him for part of the season three years ago but he came back solidly and has not looked back since.",
                     @"Age 36; hailing from one of New Zealand’s top stables and posted outstanding wins at home, in Australia and Singapore before moving to Hong Kong.   An impressive 260 wins as he goes into the 2013/14 season."];
   
 
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _racerNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"racerTableCell";
    RaceTableViewCell *cell = [tableView
                               dequeueReusableCellWithIdentifier:CellIdentifier
                               forIndexPath:indexPath];
    
    // Configure the cell...
    
    long row = [indexPath row];
    
    cell.racerNames.text= self.racerNames[row];
    cell.horseNames.text = self.horseNames[row];
    cell.racerImages.image = [UIImage imageNamed:self.racerImages[row]];
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowRacerDetails"])
    {
        RaceDetailViewController *detailViewController =
        [segue destinationViewController];
        
      NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        //NSIndexPath *myIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        detailViewController.racerDetailModel = [[NSArray alloc]
                            initWithObjects:[self.racerNames objectAtIndex:[myIndexPath row]],
                                               [self.racerCountry objectAtIndex:[myIndexPath row]],
                                                 [self.racerDescription objectAtIndex:[myIndexPath row]],
                                                 [self.racerImages objectAtIndex:[myIndexPath row]],
                                                nil];
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
