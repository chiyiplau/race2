//
//  RaceDetailViewController.h
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceDetailViewController : UIViewController
@property (strong, nonatomic) NSArray *racerDetailModel;
@property (strong, nonatomic) IBOutlet UILabel *racerNames;
@property (strong, nonatomic) IBOutlet UILabel *racerCountry;
@property (strong, nonatomic) IBOutlet UILabel *racerDescription;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;


@end
