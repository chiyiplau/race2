//
//  RaceTableViewController.h
//  RaceTable
//
//  Created by Chiyip on 24/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *racerImages;
@property (nonatomic, strong) NSArray *racerDescription;
@property (nonatomic, strong) NSArray *racerCountry;
@property (nonatomic, strong) NSArray *racerNames;
@property (nonatomic, strong) NSArray *horseNames;
@property (nonatomic, copy) NSIndexPath *path;

@property (strong, nonatomic) NSArray *raceDetailModel;
@property (strong, nonatomic) IBOutlet UILabel *racerLabel;
@property (strong, nonatomic) IBOutlet UILabel *horseLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;



@end
