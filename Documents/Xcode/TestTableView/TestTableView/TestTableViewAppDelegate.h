//
//  TestTableViewAppDelegate.h
//  TestTableView
//
//  Created by Chiyip on 31/10/13.
//  Copyright (c) 2013 Chiyip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestTableViewAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
